> Note: Develpment moved to https://gitlab.com/CodethinkLabs/lorry/trovekube

# TroveKube

A simple reimplementation of the [Baserock Trove] based on containers
to enable Docker and Kubernetes deployments.


# Using Docker Compose

We assume that you have a basic understanding of `docker` and that
you already have installed `docker` and `docker-compose` in your system.


    cd docker-compose/
    sh run.sh

> Note: You might have to run the command avobe with `sudo` if your
> user is not in the `docker` group.

Now you should be able to see:

- Lorry-controller admin interface: <http://localhost:12765/1.0/status-html>
- CGit frontend: <http://localhost:8000/>
- Non-admin Lorry-controller status page: <http://localhost:8000/lc-status.html>


# Using Kubernetes

To deploy it, start by configuring the storage NFS server. This is for now
specific to minkube.

    kubectl apply -f kubernetes/minikube-storage/
    nfs_ip=`kubectl -n trovekube get svc nfs-server -o jsonpath='{.spec.clusterIP}'`
    sed -i -e "s/NFS_SERVER_URL/$nfs_ip/" kubernetes/config-nfs.yaml

Now with the nfs server configured we can finish deploying the app:

    kubectl create -f kubernetes/

You can see some logs:

    kubectl -n trovekube logs service/lorry-controller-webapp

And all the things running:

    kubectl -n trovekube get all

If using Minikube you can access lighttpd and lorry-controller-webapp
services using:

    minikube service -n trovekube lighttpd
    minikube service -n trovekube lorry-controller-webapp


# On EKS (AWS)  with Terraform and Helm

## Configure cluster using Terraform

Decide a cluster name

    MY_CLUSTER_NAME=ironfoot

Set your AWS credentials

    export AWS_ACCESS_KEY_ID="ASDF80ASDFA90"
    export AWS_SECRET_ACCESS_KEY=1234567890&AaSsdDfF"

Apply terraform changes to deploy EKS cluster

    cd terraform
    terraform apply -var cluster_id=$MY_CLUSTER_NAME

> This step will ask you for a key name to inject into your EKS
> nodes. Using one will help with later debugging.


Get the ID and region of the recently created EFS

    efs_id=`terraform output efs_trovekube_data_id`
    efs_region=`terraform output efs_region`

Configure `kubectl` to work with the deployed cluster

    export KUBECONFIG=`pwd`/kubeconfig_k8-cluster-$MY_CLUSTER_NAME

And now go back to the parent directory

    cd ..


## Install TroveKube using Helm

The Helm chart has all the dependencies needed, it will install:
- An NGINX ingress
- Cert manager
- EFS provisioner.

If any of these dependencies are already running in the cluster, you
can disable them in the values.yaml file (`charts/trovekube/values.yaml`).

Now you can install TroveKube and dependencies

    helm install trovekube charts/trovekube/ --set efs-provisioner.efsProvisioner.efsFileSystemId=$efs_id --set efs-provisioner.efsProvisioner.awsRegion=$efs_region

> If you find this command too long, you can just modify these values
> in the `values.yaml` file, or create an extra `yaml` file with your
> variables.

Now change the DNS records of the domain name specified in `values.yaml`
to point to the `EXTERNAL-IP` of the NGINX ingress controller:

    kubectl get svc trovekube-nginx-ingress-controller

Once everything finishes setting up, you will be able to access TroveKube
using that domain name. It's time to verify the SSL certificate was setup
correctly (use any web browser to see the certificate). If the certificate
was issued by `Fake LE Intermediate X1`, then all worked correctly.

It's time now to change the `cert-manager.io/issuer` in `values.yaml` to
be `trovekube-letsencrypt-prod`, and then upgrade the deployment, running:

    helm upgrade trovekube charts/trovekube/ --set efs-provisioner.efsProvisioner.efsFileSystemId=$efs_id --set efs-provisioner.efsProvisioner.awsRegion=$efs_region

# Some notes

To install a repo in helm

    helm repo add stable https://kubernetes-charts.storage.googleapis.com/
    helm repo update

Installing efs-provisioner

    helm install my-efs-provider stable/efs-provisioner --set efsProvisioner.efsFileSystemId=$efs_id --set efsProvisioner.awsRegion=$efs_region

Install nginx ingress

    helm install my-nginx-ingress stable/nginx-ingress

See status of the nginx ingress, and get IP

    kubectl get svc


Install cert-manager

    helm repo add jetstack https://charts.jetstack.io
    helm repo update

    kubectl create namespace cert-manager
    helm install cert-manager   --namespace cert-manager   --version v0.12.0   jetstack/cert-manager

See all the services of cert-manager

    kubectl get all --namespace cert-manager

[Baserock Trove]: http://wiki.baserock.org/Trove/
