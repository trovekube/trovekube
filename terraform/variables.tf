variable "ebs_optimized" {
  default = true
}

variable "node_max_size" {
  default = 3
}

variable "node_min_size" {
  default = 3
}

variable "node_type" {
  type    = string
  default = "t3.small"
}

variable "cluster_id" {
  type = string
}

variable "s3_number" {
  default = 0
}

variable "key_pair_name" {
  type = string
}

variable "region" {
  type    = string
  default = "eu-west-1"
}
