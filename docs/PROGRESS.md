# Motivations

We don't trust the internet. Services can go down, or get compromised. We want
to be in control of that and be sure we will be able to keep consuming upstream
software even if that happens.

# Requirements

- Mirroring service
- Convert things to Git
- Permanent URLs
- Reliable, has to be up 24/7
- Ability to deploy on premises
- Has to be able to scale easily

# Current architecture

As it is today, Baserock's Trove, [git.baserock.org], is composed of various
services:

- Git
    - Git daemon - To serve the repos over git:// protocol
    - Gitano - Authorisation and authentication, for ssh and https. Lets you
      create users, and projects, and helps you manage the ssh keys of these
users and access rules for these projects
    - Lighttpd - To enable access Gitano repositories over http:// and http://
    - sshd - To enable access Gitano and Gitano repositories over ssh://
- Git browser
    - Lighttpd - To expose cgit.
    - cgit.cgi - To browse all the repos
    - cgit plugins - Modify how some of the files look (baserock definitions
      enhancements)
- Mirroring
    - Lighttpd to run the Lorry-controller-webapp service
    - Lorry-controller-webapp - Main service, stores all the data in a sqlite
      DB, Scheduler.
    - Lorry-controller-minion - Worker, does the mirroring/conversion to git,
      pushes to the git mirrors
    - Lorry-controller-readconf - Tells webapp update DB with mirroring
      configuration
    - Lorry-controller-ls-troves - Tells webapp to updates scheduler with
      upstream trove configuration (A trove can mirror an entire trove)
    - Lorry-controller cleanup jobs - cleans status of killed jobs, and kills
      ghost processes

In the following diagram we can see a (simplified) view of how these services
interact with each other.

![](images/gbo-current-arch.jpg "Current architecture")


# Architecture proposal

Given how things are split in multiple services, we can talk about a container
based solution.  Moving to a Kubernetes solution will give us various benefits:

- Elasticity, we can grow the deployment without downtime
- Replicas of the services, that will help getting high availability
- Multi-region support? (said by Edmund)
- "Simple" base OS upgrades
  <https://docs.aws.amazon.com/eks/latest/userguide/migrate-stack.html>
- ...

The current architecture can be simplified by not including Gitano. In my
opinion, if this deployment is not going to host repositories (as we used to do
with Baserock), and will just be a mirroring service, then including Gitano to
support authentication doesn't make sense.


In this diagram we can see the (simplified) proposed architecture:


![](images/gbo-new-arch.jpg "New architecture")


## Services or pods

Kubernetes will require a single service running on each of the containers/pods.
The set of services (pods) we would need to implement the proposed architecture
are:

Persistent services
- Git daemon (optional)
- Lighttpd with cgit or any other
- Lorry-controller-webapp
- Lorry-controller-minion

Timer jobs (cronJob)

- Lorry-controller-readconf
- Lorry-controller-ls-troves
- LC Status

## Volumes Some of these will need access to some sort of storage backend in
order to work:

- **Mirroring volume**: (a.k.a. Working area) Where we store our mirroring
  cache. This is our more precious data for some of the mirrors we currently
  support, like bzr and cvs. The mirroring and conversion for these 2 will not
  produce the same git SHAs. For this, is very important to keep the previous
  conversion artifacts to ensure that at leas the current converted history is
  kept the same.
- **Git volume**. Where all our git repositories live.
- **Webapp database**: Could go into a different volume, or we could reuse the
  Mirroring one. Having a separate one might make sense as this sqlite database
 has gone corrupt in some cases, being the best solution to wipe it.


## Data access

- Git daemon, cgit, and minions will have access to the Git volume
- Minions will have access to the Mirroring volume.
- Lorry-controller-webapp will have access only to the Webapp database


## Monitoring

Something we currently don't have in our current implementation, is good
monitoring. Moving to Kubernetes means that we will be losing access to the only
information we have at the moment: tools like `top`, `free`, `ps`, and access to
all the logs.

To solve that situation we will have to put in place some logging and monitoring
services. Presumably an ELK stack and Prometheus.


## Reliability

To make sure we are online all the time, we can't rely on a single cloud. We
will be running this in 2 different providers than can give us Kubernetes as a
service. The ones we are thinking about are AWS (EKS) and GCP (GKS).

For this to work we will need:

- A level 3 load balancer (Amazon AS)
- Data replication between the 2 services

To ensure the data is replicated across all regions within the same provider, we
will make use of GFS and EFS from Goolgle and AWS.


## Smaller deployments (downstream)

Nowadays anybody can have their own Kubernetes cluster, and anybody will be able
to deploy the service in their own organisation.


## Other considerations

Mirroring worker pods don't have to live in different regions, the only services
that make sense to put into different regions are the ones serving the git
repositories. We might want to put replicas in different regions to make sure
mirroring keeps happening if one region goes down BUT then we need to think
about where the webapp service lives, or if we need to have replicas, etc.

Changing some of the services to not use Gitano should be straightforward.

Some of the current jobs may need some thinking:

- Lorry-controller clean-up jobs might need to change. The status update of the
  finished jobs might be difficult to figure, given that it won't have access to
 the processes running.
- Killing ghost processes may not be necessary any more.

The current architecture assumes that `lorries.git`, which contains all the
mirroring configuration it's outside of our system. That could get compromised,
so we may want to consider hosting this ourselves in an special configuration
git repository.

Sometimes, some of the upstream rewrite the history of some branches. In
development branches that's fine, and lorry-controller gives you a mechanism for
setting which branches it's allowed to force-push, using regular expressions
too. But, what happens if any of them force-pushes to `master` on purpose, to
rewrite the history? An idea would be to make `lorry` preserve the old branch
somehow, with a different name, and then recreate the new `master` branch with
the new history?


## Issues and questions

The data, within the same service provider will be easy to keep synchronised, as
it will be in most cases transparent to us. But what happens when we start
having more than one service provider? We need to synchronise the data. This is
not a problem if the mirroring only happens in one of the providers.

- Mirroring would stop if one of the providers goes down
- Can we solve this situation anyhow?


Multi-region k8s deployments might not work out of the box. This was mentioned
as an easy thing during our meeting with Edmund, but I don't know if it will be.

We might want to get rid of Sqlite.


## Work items

PoC single host: Completed

- [x] Create containers needed to run the services needed
    - [x] Adapt lorry and lorry-controller to not use Gitano
    - [x] Script Dockerfiles to create the containers
- [x] Create a docker-compose script and documentation to deploy:
    - [x] Webapp service
    - [x] Some minions
    - [x] A frontend (lighttpd with cgit)

PoC k8s single host:

- [x] Create kubernetes files to deploy the same containers created above.
- [ ] Add monitoring services
    - [ ] Prometheus
    - [ ] Kibana

PoC k8s AWS

- [ ] Create terraform scripts to create the k8s cluster
- [ ] Adapt the solution to take a FQDN

PoC k8s multiple regions

- [ ] Adapt terraform to do deployments in various regions
- [ ] To use EFS
- [ ] Rethink architecture for the single webapp situation.

PoC k8s multiple providers multiple regions

- [ ] Implement sync between clouds
- [ ] Implement a Load balancer to point to both clouds.

[git.baserock.org]: https://git.baserock.org
