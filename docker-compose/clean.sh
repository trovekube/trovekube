#!/usr/bin/env bash

set -eux

for dir in gits gits/git gits/tarballs gits/bundles working-area log html db; do
  sudo rm -rf "${dir}"
  mkdir "${dir}"
done
